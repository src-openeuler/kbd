%global kbd_datadir %{_exec_prefix}/lib/kbd

Name:           kbd
Version:        2.6.1
Release:        2
Summary:        Tools for managing Linux console(keyboard, virtual terminals, etc.)
License:        GPLv2+
URL:            https://www.kbd-project.org/
                                                                                    
Source0:        https://mirrors.edge.kernel.org/pub/linux/utils/kbd/%{name}-%{version}.tar.xz
Source1:        kbd-latsun-fonts.tar.bz2
Source2:        kbd-latarcyrheb-32.tar.bz2
Source3:        xml2lst.pl
Source4:        vlock.pamd
Source5:        kbdinfo.1
Source6:        cz-map.patch

Patch0:         kbd-1.15-keycodes-man.patch
Patch1:         kbd-1.15-sparc.patch
Patch2:         kbd-1.15-unicode_start.patch
Patch3:         kbd-1.15.5-sg-decimal-separator.patch
Patch4:         kbd-1.15.5-loadkeys-search-path.patch
Patch5:         kbd-2.0.2-unicode-start-font.patch
Patch6:         kbd-2.4.0-covscan-fixes.patch

BuildRequires:  bison flex gettext pam-devel check-devel
BuildRequires:  gcc console-setup xkeyboard-config automake 
Requires:       %{name}-misc = %{version}-%{release}
Requires:       %{name}-legacy = %{version}-%{release}
Provides:       vlock = %{version}
Conflicts:      vlock <= 1.3

%description
The %{name} project contains tools for managing Linux console ,including 
Linux console,virtual terminals, keyboard, etc, mainly, what they do 
is loading console fonts and keyboard maps.

%package 	misc
Summary:        Data for %{name} package
BuildArch:      noarch
 
%description 	misc
The %{name}-misc package contains data for %{name} package,including console fonts,
keymaps and so on. But %{name}-misc can do nothing without %{name}.

%package 	legacy
Summary:        Legacy data for %{name} package
BuildArch:      noarch
 
%description 	legacy
The %{name}-legacy package contains original keymaps for %{name} package.
But %{name}-legacy can do nothing without %{name}.

%package_help

%prep
%setup -q -a 1 -a 2
cp -fp %{SOURCE3} .
cp -fp %{SOURCE6} .
%patch0 -p1 -b .keycodes-man
%patch1 -p1 -b .sparc
%patch2 -p1 -b .unicode_start
%patch3 -p1 -b .sg-decimal-separator
%patch4 -p1 -b .loadkeys-search-path
%patch5 -p1 -b .unicode-start-font
%patch6 -p1 -b .covscan-fixes.patch
aclocal
autoconf

pushd data/keymaps/i386
cp qwerty/pt-latin9.map qwerty/pt.map
cp qwerty/sv-latin1.map qwerty/se-latin1.map

mv azerty/fr.map azerty/fr-old.map
cp azerty/fr-latin9.map azerty/fr.map

cp azerty/fr-latin9.map azerty/fr-latin0.map 

mv fgGIod/trf.map fgGIod/trf-fgGIod.map
mv olpc/es.map olpc/es-olpc.map
mv olpc/pt.map olpc/pt-olpc.map
mv qwerty/cz.map qwerty/cz-qwerty.map
popd

pushd po
rm -f gr.po gr.gmo
popd

iconv -f iso-8859-1 -t utf-8 < "ChangeLog" > "ChangeLog_"
mv "ChangeLog_" "ChangeLog"

%build
%configure --prefix=%{_prefix} --datadir=%{kbd_datadir} --mandir=%{_mandir} --localedir=%{_datadir}/locale --enable-nls
%make_build

%install
%make_install

rm -f $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/i386/qwerty/ro_win.map.gz

ln -s sr-cy.map.gz $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/i386/qwerty/sr-latin.map.gz

ln -s us.map.gz $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/i386/qwerty/ko.map.gz

ln -s fa.map.gz $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/i386/qwerty/ara.map.gz

sed -i -e 's,\<kbd_mode\>,%{_bindir}/kbd_mode,g;s,\<setfont\>,%{_bindir}/setfont,g' \
        $RPM_BUILD_ROOT%{_bindir}/unicode_start

gzip -c %SOURCE5 > $RPM_BUILD_ROOT/%{_mandir}/man1/kbdinfo.1.gz

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pam.d
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/vlock

mkdir -p $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/legacy
mv $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/{amiga,atari,i386,include,mac,ppc,sun} $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/legacy

mkdir -p $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb
perl xml2lst.pl < /usr/share/X11/xkb/rules/base.xml > layouts-variants.lst
while read line; do
  XKBLAYOUT=`echo "$line" | cut -d " " -f 1`
  echo "$XKBLAYOUT" >> layouts-list.lst
  XKBVARIANT=`echo "$line" | cut -d " " -f 2`
  ckbcomp -rules base "$XKBLAYOUT" "$XKBVARIANT" | gzip > $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb/"$XKBLAYOUT"-"$XKBVARIANT".map.gz
done < layouts-variants.lst

cat layouts-list.lst | sort -u >> layouts-list-uniq.lst
while read line; do
  ckbcomp -rules base "$line" | gzip > $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb/"$line".map.gz
done < layouts-list-uniq.lst

zgrep -L "U+0041" $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb/* | xargs rm -f

if [ -f "$RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb/cz.map.gz" ]; then
  gunzip $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb/cz.map.gz
  patch $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb/cz.map < %{SOURCE6}
  gzip -n $RPM_BUILD_ROOT%{kbd_datadir}/keymaps/xkb/cz.map
fi

%find_lang %{name}

%check
make check

%files 
%defattr(-,root,root)
%license COPYING
%doc AUTHORS
%{_datadir}/locale/*
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/pam.d/vlock
# library for tests
%exclude %{_libdir}/libtswrap*
%exclude %{_prefix}/lib/debug/%{_libdir}/libtswrap*

%files misc
%defattr(-,root,root)	
%{kbd_datadir}
%exclude %{kbd_datadir}/keymaps/legacy

%files legacy
%defattr(-,root,root)
%{kbd_datadir}/keymaps/legacy

%files help
%defattr(-,root,root)
%doc ChangeLog README docs/doc/%{name}.FAQ*.html docs/doc/font-formats/*.html docs/doc/utf/utf* docs/doc/dvorak/*
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/keymaps.5.gz
%{_mandir}/man8/*.8.gz

%changelog
* Thu May 30 2024 zhangpan <zhangpan103@h-partners.com> - 2.6.1-2
- resolve gzip time eliminate differences

* Wed Jul 19 2023 zhouwenpei <zhouwenpei@h-partners.com> - 2.6.1-1
- update to 2.6.1

* Tue Apr 18 2023 liweiganga <liweiganga@uniontech.com> - 2.5.1-2
- update patch adding compose rules for converted cz layout

* Thu Nov 03 2022 zhouwenpei <zhouwenpei@h-partners.com> - 2.5.1-1
- update to 2.5.1

* Tue Dec 07 2021 wangkerong <wangkerong@huawei.com> - 2.4.0-1
- update to 2.4.0

* Mon Jul 12 2021 panchenbo <panchenbo@uniontech.com> - 2.2.0-2
- fix kbd contain man dir

* Tue Jul 28 2020 hanhui <hanhui15@huawei.com> - 2.2.0-1
- update to 2.2.0

* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.4-11
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:bugfix in build process

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.4-10
- Package init
